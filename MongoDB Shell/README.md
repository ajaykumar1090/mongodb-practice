### Data Types

MongoDB has several built-in data types that can be used to represent different types of values in documents stored in a collection.

#### 1. String

Represents a sequence of UTF-8 characters.

```javascript
"Hello, MongoDB!";
```

#### 2. Integer

Represents a 32-bit signed integer.

```javascript
42;
```

#### 3. Double

Represents a 64-bit double-precision floating-point value.

```javascript
3.14;
```

#### 4. Boolean

Represents a boolean value.

```javascript
true;
```

#### 5. Array

Represents an ordered list of values.

```javascript
[1, 2, 3, 4];
```

#### 6. Object

Represents a document with fields and values.

```javascript
{ key1: "value1", key2: "value2" }
```

#### 7. ObjectId

Represents a unique identifier typically used as the default **\_id** field for documents.

```javascript
ObjectId("5fd31f7b1c8a4b0490d3efb5");
```

#### 8. Date

Represents a date and time

```javascript
Date(); // this method which returns the current date as a string

new Date(); // this constructor which returns a Date object using the ISODate()

ISODate("2022-01-01T12:00:00Z"); // this constructor which returns a Date object using the ISODate() wrapper.
```

#### 9. Null

Represents the absence of a value.

```javascript
null;
```

#### 10. Embedded Document

```javascript
{ name: "John", address: { city: "New York", zip: "10001" } }
```

#### 11. Binary Data

Represents binary data, where the first parameter is the binary subtype.

```javascript
BinData(0, "base64_encoded_data");
```

#### 12. Regular Expression

Represents a regular expression.

```javascript
/pattern/i;
```

#### 13. MinKey and MaxKey

```javascript
MinKey();
MaxKey();
```

```javascript
db.example_collection.insertOne({
  string: "Hello, MongoDB!",
  integer: 42,
  double: 3.14,
  boolean: true,
  array: [1, 2, 3],
  object: { name: "John", age: 25 },
  objectId: ObjectId(),
  date: ISODate("2022-01-05T10:30:00Z"),
  null: null,
  regex: /pattern/i,
  binary: BinData(0, "base64encodeddata"),
});
```

### Type Checking

In MongoDB, there is no direct equivalent to the JavaScript instanceof operator or the typeof operator when querying documents in a collection.

#### 1. Using $type Operator

The **$type** operator in MongoDB allows you to query documents based on the BSON type of a field.

```javascript
// Find documents where the 'age' field is of type number
db.your_collection_name.find({ age: { $type: "number" } });

// Find documents where the 'name' field is of type string
db.your_collection_name.find({ name: { $type: "string" } });
```

#### 2. Using $exists Operator

The **$exists** operator allows you to check whether a field exists in a document, which indirectly checks if the field has a specific type:

```javascript
// Find documents where the 'age' field exists and is not null
db.your_collection_name.find({ age: { $exists: true, $ne: null } });

// Find documents where the 'name' field exists and is not an empty string
db.your_collection_name.find({ name: { $exists: true, $ne: "" } });
```

#### 3. Using Aggregation Framework

In MongoDB's Aggregation Framework, you can also perform more complex type-related operations:

```javascript
// Find documents where the 'age' field is of type number
db.your_collection_name.aggregate([{ $match: { age: { $type: "number" } } }]);
```

> These examples demonstrate how you can filter documents based on their field types in MongoDB queries. But MongoDB itself doesn't enforce strict data types, so these checks are based on the BSON types used to store the data.
