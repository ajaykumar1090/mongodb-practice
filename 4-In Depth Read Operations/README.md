# Read (Query) Operations

We can perform various **read (query)** operations to retrieve data from a collection.

> MongoDB provides a rich set of **query** and **projection** operators to filter and shape the data you retrieve.

### 1. findOne() Method

### 2. find() Method

The **find()** method is used to query documents in a collection. It returns a cursor to the selected documents.

```javascript
// Find all documents in a collection
db.collectionName.find();

// Find documents that match a specific condition
db.collectionName.find({ `<query>` }, {`<projection>`});
```

- **Query:** It's optional. Specify selection filters using query operators.
- **Projection:** It's optional. Specify the field to return from the documents.

```javascript
// Find all documents in the 'users' collection
db.users.find();

// Find users with age greater than 25
db.users.find({ name: "Adam", age: { $gt: 25 } });
```

### 3. Query Nested or Embedded Documents

To query for **embedded (nested)** documents in MongoDB, you can use **dot** notation to access fields within nested documents.

Create a collection named "inventory" with documents containing embedded size.

```javascript
db.inventory.insertMany([
  { item: "journal", qty: 25, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
  { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "A" },
  { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
  { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
  { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" },
]);
```

#### Match an Embedded/Nested documents as below -

To specify equality condition, provide **exact value** of the nested field

```javascript
db.inventory.find({ size: { h: 14, w: 21, uom: "cm" } });
```

#### Query by Nested Field

To specify a query condition on fields in an **embedded/nested** document, use dot notation
`("field.nestedField")`

```javascript
db.inventory.find({ "size.h": 14 });

db.inventory.find({ "size.uom": "in" });

db.inventory.find({ "size.h": { $lt: 15 } });
```

#### Query using Logical Operators

Find users less than 15 with a specific status.

```javascript
db.inventory.find({ "size.h": { $lt: 15 }, "size.uom": "in" });

db.inventory.find({ "size.h": { $lt: 15 }, "size.uom": "in", status: "D" });
```

### 4. Query an Array Field in Documents

We can use various query operators to search for documents based on the elements within the arrays.

```javascript
db.inventory.insertMany([
  { item: "journal", qty: 25, tags: ["blank", "red"], dim_cm: [14, 21] },
  { item: "notebook", qty: 50, tags: ["red", "blank"], dim_cm: [14, 21] },
  { item: "paper", qty: 100, tags: ["red", "blank", "plain"], dim_cm: [14, 21] },
  { item: "planner", qty: 75, tags: ["blank", "red"], dim_cm: [22.85, 30] },
  { item: "postcard", qty: 45, tags: ["blue"], dim_cm: [10, 15.25] },
  { item: "pen", qty: 30, tags: ["blue", "red"], dim_cm: [10, 20] },
  { item: "pencil", qty: 20, tags: ["red", "blue"], dim_cm: [12, 22.2] },
]);
```

#### Match an Array

To specify **match** condition on an array, provide exact value of array field.

```javascript
db.inventory.find({ tags: ["blank", "red"] });
```

#### Query an array for an Element

To query an array field contains at least one element with the specified value.

```javascript
db.inventory.find({ tags: "red" });

db.inventory.find({ dim_cm: { $gt: 25 } });
```

#### Match any in Array

Find products with any of the specified tags.

```javascript
db.inventory.find({ tags: { $in: ["plain", "blue"] } });
```

#### Match All in Array

Find products with all of the specified tags.

```javascript
db.inventory.find({ tags: { $all: ["blue", "red"] } });
```

#### Size of Array

Find products with a specific number of tags.

```javascript
db.inventory.find({ tags: { $size: 2 } });
```

#### Array Element at a Specific Position

Find products where the second tag is "blank".

```javascript
db.inventory.find({ "tags.1": "blank" });
```

#### Query for Array Elements with a Condition

Find products with tags containing the letter 'pl'.

```javascript
db.inventory.find({ tags: { $elemMatch: { $regex: /pl/ } } });
```

#### Projection for Array Fields

Select only certain fields in the array.

```javascript
db.inventory.find({ tags: "red" }, { item: 1, "tags.$": 1, _id: 0 });
```

### 5. Query an Array of Embedded Documents

We can use various query operators to search for documents based on the elements within the **array of embedded** documents.

```javascript
db.inventory.insertMany([
  {
    item: "journal",
    instock: [
      { warehouse: "A", qty: 5 },
      { warehouse: "C", qty: 15 },
    ],
  },
  { item: "notebook", instock: [{ warehouse: "C", qty: 5 }] },
  {
    item: "paper",
    instock: [
      { warehouse: "A", qty: 60 },
      { warehouse: "B", qty: 15 },
    ],
  },
  {
    item: "planner",
    instock: [
      { warehouse: "A", qty: 40 },
      { warehouse: "B", qty: 5 },
    ],
  },
  {
    item: "postcard",
    instock: [
      { warehouse: "B", qty: 15 },
      { warehouse: "C", qty: 35 },
    ],
  },
]);
```

### Projection

### Sorting

### Limit and Skip

### Count Documents
