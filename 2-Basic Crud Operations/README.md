# Database and Collection Creation

#### Create a Database

To create a new **database** or **switch** to an existing one, use the **use** command. If the specified database does not exist, MongoDB will create it when you insert data.

## CRUD

CRUD operations (Create, Read, Update, Delete) are fundamental to interacting with databases. In MongoDB, a NoSQL database, you perform CRUD operations on documents in collections.

### 1.Creating (Inserting) Operation:

Create & add new documents to the collections.

### 2. Reading (Querying) Operation:

Retrive data from collections.

### 3. Updating Operation:

Modify existing documents in the collections.

### 4. Deleting Operation:

Delete or remove documents from the collections.
