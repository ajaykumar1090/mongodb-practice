# $match Stage

The **$match** stage is used to filter documents based on specified criteria. It allows you to select a subset of documents that match the specified conditions.

```javascript
// Filters documents where the specified field equals the specified value.
{ $match: { field: value } }

// Filters documents where the specified field is greater than the specified value.
{ $match: { field: { $gt: value } } }

// Filters documents where the specified field matches any value in the specified array.
{ $match: { field: { $in: [value1, value2, ...] } } }
```

#### Simple Equality:

```javascript
db.sales.aggregate([{ $match: { product: "Apples" } }]);

db.articles.aggregate([{ $match: { author: "dave" } }]);
```

#### Greater Than:

```javascript
db.sales.aggregate([{ $match: { quantity: { $gt: 10 } } }]);

db.orders.aggregate([{ $match: { totalAmount: { $gt: 1000 } } }]);
```

#### In Array

```javascript
db.users.aggregate([{ $match: { interests: { $in: ["music", "sports"] } } }]);

db.users.aggregate([{ $match: { role: { $in: ["admin", "manager"] } } }]);
```

#### Logical AND/Combining Conditions:

```javascript
db.orders.aggregate([{ $match: { status: "shipped", totalAmount: { $gte: 1000 } } }]);

db.inventory.aggregate([{ $match: { category: "Electronics", inStock: true } }]);
```

#### Date Range

```javascript
db.events.aggregate([
  {
    $match: {
      eventDate: { $gte: ISODate("2023-01-01"), $lt: ISODate("2023-02-01") },
    },
  },
]);

db.orders.aggregate([
  {
    $match: {
      orderDate: { $gte: ISODate("2023-02-01"), $lt: ISODate("2023-03-01") },
    },
  },
]);
```

#### Logical OR

```javascript
db.products.aggregate([{ $match: { $or: [{ price: { $gte: 500 } }, { category: "Luxury" }] } }]);
```

> To optimize the aggregation query, place the **$match** in starting stages.

## $group Stage

The **$group** stage is used to group documents based on a specified key and perform aggregate operations on the grouped data.

```javascript
{
 $group: {
     _id: <expression>, // Group key
     <field1>: { <operator1> : <expression1> },
     ...
   }
 }
```

#### Group Documents

```javascript
db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      totalQuantity: { $sum: "$quantity" },
    },
  },
]);
```

#### Calculate Count, Sum, and Average

```javascript
db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      totalQuantity: { $sum: "$quantity" },
      averagePrice: { $avg: "$price" },
      totalSaleAmount: { $sum: { $multiply: ["$quantity", "$price"] } },
      count: { $sum: 1 },
    },
  },
]);
```

#### Group by Item Having

```javascript
db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      totalQuantity: { $sum: "$quantity" },
    },
  },
  {
    $match: {
      totalQuantity: { $gte: 20 },
    },
  },
]);
```

First stage group the documents by product and second stage filter the documents by "totalQuantity" greater than or equal to 20.

#### Group by Day of the Year

```javascript
db.sales.aggregate([
  {
    $match: {
      saleDate: { $gte: "2024-01-01", $lt: "2025-01-01" },
    },
  },
  {
    $group: {
      _id: "$saleDate",
      totalSalesAmount: { $sum: { $multiply: ["$price", "$quantity"] } },
      averageQuantity: { $avg: "$quantity" },
      count: { $sum: 1 },
    },
  },
  {
    $sort: { totalSalesAmount: 1 },
  },
]);
```

- First stage filter the documents from year 2023 to 2024.
- Second stage group the documents by date and calculate the total sale amount, average quantity, and total count of the documents in each group.
- Third stage sorts the result by total sale amount, average quantity, and total count of the documents in each group.

#### Get Total Sale Amount in Date Range

```javascript
db.sales.aggregate([
  {
    $match: {
      saleDate: { $gte: "2024-01-01", $lt: "2025-01-01" },
    },
  },
  {
    $group: {
      _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
      totalSalesAmount: { $sum: { $multiply: ["$price", "$quantity"] } },
      averageQuantity: { $avg: "$quantity" },
      count: { $sum: 1 },
    },
  },
]);
```

#### Group by null

```javascript
db.sales.aggregate([
  {
    $group: {
      _id: null,
      totalSaleAmount: { $sum: { $multiply: ["$price", "$quantity"] } },
      averageQuantity: { $avg: "$quantity" },
      count: { $sum: 1 },
    },
  },
]);
```

#### Pivot the Data

```javascript
db.books.aggregate([
  {
    $group: {
      _id: "$author",
      books: { $push: "$title" },
      totalCopies: { $sum: "$copies" },
    },
  },
]);
```

#### Group Entire Documents ($$ROOT)

```javascript
db.books.aggregate([
  {
    $group: {
      _id: "$author",
      books: { $push: "$$ROOT" },
      totalCopies: { $sum: "$copies" },
    },
  },
]);

// OR

db.books.aggregate([
  {
    $group: {
      _id: "$author",
      books: { $push: "$$ROOT" },
    },
  },
  {
    $addFields: {
      totalCopies: { $sum: "$books.copies" },
    },
  },
]);
```

> Some common operators used with groups include:

- **$sum:** Calculates the sum of numeric values for each group.

```javascript
{
  $group: {
    _id: "$category",
    totalQuantity: { $sum: "$quantity" }
  }
}

db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      totalSalesAmount: { $sum: {$multiply:["$quantity","$price"]} },
    },
  },
]);
```

- **$avg:** Calculates the average of numeric values for each group.

```javascript
{
  $group: {
    _id: "$category",
    averagePrice: { $avg: "$price" }
  }
}

db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      averagePrice: { $avg: "$price" },
    },
  },
]);
```

- **$min and $max:** Finds the minimum and maximum values within each group.

```javascript
{
  $group: {
    _id: "$category",
    minPrice: { $min: "$price" },
    maxPrice: { $max: "$price" }
  }
}

db.inventory.aggregate([
  {
    $group: {
      _id: "$category",
      totalSalesAmount: { $sum: { $multiply: ["$quantity", "$price"] } },
      count: { $sum: 1 },
      minPrice: { $min: "$price" },
      maxPrice: { $max: "$price" },
    },
  },
]);
```

- **$first and $last:** Returns the first or last document within each group based on the order.

```javascript
{
  $group: {
    _id: "$category",
    firstSale: { $first: "$$ROOT" },
    lastSale: { $last: "$$ROOT" }
  }
}

db.sales.aggregate([
  {
    $group: {
      _id: "product",
      firstSale: { $first: "$$ROOT" },
      lastSale: { $last: "$$ROOT" },
    },
  },
]);
```

- **$push:** Collects values into an array within each group.

```javascript
{
  $group: {
    _id: "$category",
    products: { $push: "$product" },
    uniqueProducts: { $addToSet: "$product" }
  }
}

db.products.aggregate([
  {
    $group:{
      _id:"$category",
      products:{$push:"$product"},
      count:{$sum:1}
    }
  }
])
```

- **$addToSet:** Collects values into an array of unique expression values for each group.

```javascript
db.products.aggregate([
  {
    $group: {
      _id: "$category",
      products: { $push: "$product" },
      uniqueProducts: { $addToSet: "$product" },
      count: { $sum: 1 },
    },
  },
]);
```
