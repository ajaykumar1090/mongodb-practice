# Aggregation

MongoDB's aggregation framework is a powerful tool for analyzing and transforming data within your collections.

## 1. Aggregation Pipeline Stages

- Aggregation in MongoDB is performed using a pipeline. Each stage in the pipeline represents a specific operation.
- Stages are processed sequentially, and the result of one stage is passed as input to the next.

```javascript
db.collection.aggregate( [ { <stage 1> }, { <stage 2> }, ... ] )
```

### List of Pipeline Stages

- $addFields
- $bucket
- $bucketAuto
- $changeStream
- $changeStreamSplitLargeEvent
- $collStats
- [$count:]
- $densify
- $documents
- $facet
- $fill
- $geoNear
- $graphLookup
- [$group:]() Groups documents by a specified key and performs aggregations within each group.
- $indexStats
- $limit: Limits the number of documents in the output.
- $listSampledQueries
- $listSearchIndexes
- $listSessions
- $lookup
- [$match:]() Filters documents based on specified criteria.
- $merge
- $out
- $planCacheStats
- [$project:] Shapes the output document by including/excluding fields or creating new fields.
- $redact
- $replaceRoot
- $replaceWith
- $sample
- $search
- $searchMeta
- $set
- $setWindowFields
- $skip
- $sort: Orders the documents based on specified fields.
- $sortByCount
- $unionWith
- $unset
- $unwind

#### Example Pipeline

```javascript
db.collection.aggregate([
  { $match: { field: { $gte: 20 } } },
  { $group: { _id: "$category", total: { $sum: "$quantity" } } },
  { $sort: { total: -1 } },
  { $limit: 5 },
  { $project: { _id: 0, category: "$_id", totalQuantity: "$total" } },
]);
```

### Single Purpose Aggregation Methods

- db.collection.estimatedDocumentCount()
- db.collection.count()
- db.collection.distinct()

### Stages Available for Updates

### Nested Aggregation

## 2. Aggregation Pipeline Operators
