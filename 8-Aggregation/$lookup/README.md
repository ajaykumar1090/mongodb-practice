# $lookup stage

The **$lookup** stage is used to perform a left outer join to a collection in the same database. This stage allows you to combine documents from two collections based on a specified condition.

```javascript
db.collection.aggregate([
  {
    $lookup: {
      from: "otherCollection", // Target collection
      localField: "currentCollectionField", // Field from the input documents
      foreignField: "otherCollectionField", // Field from the target documents
      as: "aliasForMergedDocuments", // Output array field containing the joined documents
    },
  },
]);
```

```javascript
db.properties.aggregate([
  {
    $lookup: {
      from: "propertyphotos",
      localField: "photos",
      foreignField: "_id",
      as: "photos",
    },
  },
  {
    $project: {
      propertyName: 1,
      category: 1,
      photos: 1,
    },
  },
]);
```
