# $group Stage

The **$group** stage is used to group documents based on a specified key and perform aggregate operations on the grouped data.

```javascript
{
 $group: {
     _id: <expression>, // Group key
     <field1>: { <operator1> : <expression1> },
     ...
   }
 }
```

### Group Documents

```javascript
db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      totalQuantity: { $sum: "$quantity" },
    },
  },
]);
```

### Calculate Count, Sum, and Average

```javascript
db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      totalQuantity: { $sum: "$quantity" },
      averagePrice: { $avg: "$price" },
      totalSaleAmount: { $sum: { $multiply: ["$quantity", "$price"] } },
      count: { $sum: 1 },
    },
  },
]);
```

### Group by Item Having

```javascript
db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      totalQuantity: { $sum: "$quantity" },
    },
  },
  {
    $match: {
      totalQuantity: { $gte: 20 },
    },
  },
]);
```

First stage group the documents by product and second stage filter the documents by "totalQuantity" greater than or equal to 20.

### Group by Day of the Year

```javascript
db.sales.aggregate([
  {
    $match: {
      saleDate: { $gte: "2024-01-01", $lt: "2025-01-01" },
    },
  },
  {
    $group: {
      _id: "$saleDate",
      totalSalesAmount: { $sum: { $multiply: ["$price", "$quantity"] } },
      averageQuantity: { $avg: "$quantity" },
      count: { $sum: 1 },
    },
  },
  {
    $sort: { totalSalesAmount: 1 },
  },
]);
```

- First stage filter the documents from year 2023 to 2024.
- Second stage group the documents by date and calculate the total sale amount, average quantity, and total count of the documents in each group.
- Third stage sorts the result by total sale amount, average quantity, and total count of the documents in each group.

### Get Total Sale Amount in Date Range

```javascript
db.sales.aggregate([
  {
    $match: {
      saleDate: { $gte: "2024-01-01", $lt: "2025-01-01" },
    },
  },
  {
    $group: {
      _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
      totalSalesAmount: { $sum: { $multiply: ["$price", "$quantity"] } },
      averageQuantity: { $avg: "$quantity" },
      count: { $sum: 1 },
    },
  },
]);
```

### Group by null

```javascript
db.sales.aggregate([
  {
    $group: {
      _id: null,
      totalSaleAmount: { $sum: { $multiply: ["$price", "$quantity"] } },
      averageQuantity: { $avg: "$quantity" },
      count: { $sum: 1 },
    },
  },
]);
```

### Pivot the Data

```javascript
db.books.aggregate([
  {
    $group: {
      _id: "$author",
      books: { $push: "$title" },
      totalCopies: { $sum: "$copies" },
    },
  },
]);
```

### Group Entire Documents ($$ROOT)

```javascript
db.books.aggregate([
  {
    $group: {
      _id: "$author",
      books: { $push: "$$ROOT" },
      totalCopies: { $sum: "$copies" },
    },
  },
]);

// OR

db.books.aggregate([
  {
    $group: {
      _id: "$author",
      books: { $push: "$$ROOT" },
    },
  },
  {
    $addFields: {
      totalCopies: { $sum: "$books.copies" },
    },
  },
]);
```

#### Operators used with groups include:

- **$accumulator:** Returns the ...

- **$addToSet:** Collects values into an array of unique expression values for each group.

```javascript
db.products.aggregate([
  {
    $group: {
      _id: "$category",
      products: { $push: "$product" },
      uniqueProducts: { $addToSet: "$product" },
      count: { $sum: 1 },
    },
  },
]);
```

- **$avg:** Calculates the average of numeric values for each group.

```javascript
{
  $group: {
    _id: "$category",
    averagePrice: { $avg: "$price" }
  }
}

db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      averagePrice: { $avg: "$price" },
    },
  },
]);
```

- **$bottom:** Returns the...

- **$bottomN:** Returns the...

- **$count:** Returns the number of documents in a group.

- **$first and $last:** Returns the first or last document within each group based on the order.

```javascript
{
  $group: {
    _id: "$category",
    firstSale: { $first: "$$ROOT" },
    lastSale: { $last: "$$ROOT" }
  }
}

db.sales.aggregate([
  {
    $group: {
      _id: "product",
      firstSale: { $first: "$$ROOT" },
      lastSale: { $last: "$$ROOT" },
    },
  },
]);
```

- **$firstN and lastN:** Returns the ...

- **$min and $max:** Finds the minimum and maximum values within each group.

```javascript
{
  $group: {
    _id: "$category",
    minPrice: { $min: "$price" },
    maxPrice: { $max: "$price" }
  }
}

db.inventory.aggregate([
  {
    $group: {
      _id: "$category",
      totalSalesAmount: { $sum: { $multiply: ["$quantity", "$price"] } },
      count: { $sum: 1 },
      minPrice: { $min: "$price" },
      maxPrice: { $max: "$price" },
    },
  },
]);
```

- **maxN:** Returns the ...

- **median:** Returns the ...

- **mergeObjects:** Returns the ...

- **percentile:** Returns the ...

- **$push:** Collects values into an array within each group.

```javascript
{
  $group: {
    _id: "$category",
    products: { $push: "$product" },
    uniqueProducts: { $addToSet: "$product" }
  }
}

db.products.aggregate([
  {
    $group:{
      _id:"$category",
      products:{$push:"$product"},
      count:{$sum:1}
    }
  }
])
```

- **stdDevPop:** Returns the ...

- **stdDevSamp:** Returns the ...

- **$sum:** Calculates the sum of numeric values for each group.

```javascript
{
  $group: {
    _id: "$category",
    totalQuantity: { $sum: "$quantity" }
  }
}

db.sales.aggregate([
  {
    $group: {
      _id: "$product",
      totalSalesAmount: { $sum: {$multiply:["$quantity","$price"]} },
    },
  },
]);
```

- **top:** Returns the ...

- **topN:** Returns the ...
