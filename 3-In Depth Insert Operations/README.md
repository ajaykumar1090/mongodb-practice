# Insert Operations

In MongoDB, you can perform insert operations to add documents to a collection.

There are several ways to insert documents:

### 1. Insert a Single Document

To insert a single document into a collection, you can use the **insertOne()** method.

```javascript
db.collectionName.insertOne({ key: "value", anotherKey: "anotherValue"... });

db.users.insertOne({ name: "John Doe", age: 30, email: "john@example.com" });
```

### 2. Insert Multiple Documents

To insert multiple documents into a collection, you can use the **insertMany()** method.

```javascript
db.collectionName.insertMany([
  { key1: "value1", key2: "value2" },
  { key1: "value3", key2: "value4" },
  // ... additional documents
]);

// Examples
db.users.insertMany([
  { name: "Alice", age: 25, email: "alice@example.com" },
  { name: "Bob", age: 35, email: "bob@example.com" },
  { name: "Eve", age: 28, email: "eve@example.com" },
]);
```

### 3. Insert with \_id Field

If you want to specify the **\_id** field when inserting a document, you can include it in the document.

```javascript
db.collectionName.insertOne({ _id: "customId", key: "value" });
```

### 4. Insert with Write Concern

You can specify the write concern for insert operations, indicating the level of acknowledgment requested from MongoDB.

```javascript
db.collectionName.insertOne({ key: "value" }, { writeConcern: { w: "majority" } });
```

### 5. Insert with writeResult Object:

The **insertOne** and **insertMany** methods return a **writeResult** object that contains information about the operation, including the number of documents inserted.

```javascript
const result = db.collectionName.insertOne({ key: "value" });
print("Inserted document with ID: ", result.insertedId);
```

### 6. Insert Embedded Documents

We can use the **BSON (Binary JSON)** format to store embedded documents. BSON allows us to represent complex data structures, including arrays and nested documents.

```javascript
// Example document with an embedded address
var userDocument = {
  name: "John Doe",
  age: 25,
  address: {
    street: "123 Main Street",
    city: "Cityville",
    state: "State",
    zip: "12345",
  },
};

// Insert the document into the "users" collection
db.users.insert(userDocument);
```

You can also insert multiple documents at once using the **insertMany** method:

```javascript
db.inventory.insertMany([
  { item: "journal", qty: 25, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
  { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "A" },
  { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
  { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
  { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" },
]);
```

### 6. Insert Array Field Documents

Inserting documents with array fields in MongoDB is similar to inserting regular documents. You just need to provide the array field along with other fields.

```javascript
db.inventory.insertMany([
  { item: "journal", qty: 25, tags: ["blank", "red"], dim_cm: [14, 21] },
  { item: "notebook", qty: 50, tags: ["red", "blank"], dim_cm: [14, 21] },
  { item: "paper", qty: 100, tags: ["red", "blank", "plain"], dim_cm: [14, 21] },
  { item: "planner", qty: 75, tags: ["blank", "red"], dim_cm: [22.85, 30] },
  { item: "postcard", qty: 45, tags: ["blue"], dim_cm: [10, 15.25] },
]);
```

### 7. Insert Array of Embedded Documents

```javascript
db.inventory.insertMany([
  {
    item: "journal",
    instock: [
      { warehouse: "A", qty: 5 },
      { warehouse: "C", qty: 15 },
    ],
  },
  { item: "notebook", instock: [{ warehouse: "C", qty: 5 }] },
  {
    item: "paper",
    instock: [
      { warehouse: "A", qty: 60 },
      { warehouse: "B", qty: 15 },
    ],
  },
  {
    item: "planner",
    instock: [
      { warehouse: "A", qty: 40 },
      { warehouse: "B", qty: 5 },
    ],
  },
  {
    item: "postcard",
    instock: [
      { warehouse: "B", qty: 15 },
      { warehouse: "C", qty: 35 },
    ],
  },
]);
```
